The System Validation project consists of the following files:

1. Report: The report gives a general overview of the system being modelled (Railroad Crossing). It also gives detailed explanation 
           of the system architecture and the requirements of the system. The requirements in the report have been stated formally 
           and have also been translated into mu-calulus.

2. behavior_specification.mcrl2: This is the source file for modelling the Railroad System. This file contains the modelling of system architecture porposed in
               report in mcrl2 functional language. The different states of the system can be viewed by running this source file in the mcrl2
               program. A 3D model can also be developed for checking the symmetry of the model.

3. requirements.mcf: This file consists of the requirements being translated into mu-calulus (Hennnesy Milner Logic). 
                     This file is loaded when the the mcrl2.lps file is being translated into mcrl2.pbes. The output of
		     of the mcrl2.pbes is boolean which justifies whether the requirements is satisfied by the system. 
